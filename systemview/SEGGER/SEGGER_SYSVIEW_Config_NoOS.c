/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 1995 - 2019 SEGGER Microcontroller GmbH             *
*                                                                    *
*       www.segger.com     Support: support@segger.com               *
*                                                                    *
**********************************************************************
*                                                                    *
*       SEGGER SystemView * Real-time application analysis           *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* SEGGER strongly recommends to not make any changes                 *
* to or modify the source code of this software in order to stay     *
* compatible with the RTT protocol and J-Link.                       *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* conditions are met:                                                *
*                                                                    *
* o Redistributions of source code must retain the above copyright   *
*   notice, this list of conditions and the following disclaimer.    *
*                                                                    *
* o Redistributions in binary form must reproduce the above          *
*   copyright notice, this list of conditions and the following      *
*   disclaimer in the documentation and/or other materials provided  *
*   with the distribution.                                           *
*                                                                    *
* o Neither the name of SEGGER Microcontroller GmbH         *
*   nor the names of its contributors may be used to endorse or      *
*   promote products derived from this software without specific     *
*   prior written permission.                                        *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED. IN NO EVENT SHALL SEGGER Microcontroller BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
**********************************************************************
*                                                                    *
*       SystemView version: V2.52h                                    *
*                                                                    *
**********************************************************************
-------------------------- END-OF-HEADER -----------------------------

File    : SEGGER_SYSVIEW_Config_NoOS.c
Purpose : Sample setup configuration of SystemView without an OS.
Revision: $Rev: 12706 $
*/
#include "SEGGER_SYSVIEW.h"
#include "SEGGER_SYSVIEW_Conf.h"

// SystemcoreClock can be used in most CMSIS compatible projects.
// In non-CMSIS projects define SYSVIEW_CPU_FREQ.
extern unsigned int SystemCoreClock;

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
// The application name to be displayed in SystemViewer
#define SYSVIEW_APP_NAME        "MCU_CPP_test"

// The target device name
#define SYSVIEW_DEVICE_NAME     "Cortex-M7F"

// Frequency of the timestamp. Must match SEGGER_SYSVIEW_Conf.h
#define SYSVIEW_TIMESTAMP_FREQ  (SystemCoreClock)

// System Frequency. SystemcoreClock is used in most CMSIS compatible projects.
#define SYSVIEW_CPU_FREQ        (SystemCoreClock)

// The lowest RAM address used for IDs (pointers)
#define SYSVIEW_RAM_BASE        (0x10000000)

// Define as 1 if the Cortex-M cycle counter is used as SystemView timestamp. Must match SEGGER_SYSVIEW_Conf.h
#ifndef   USE_CYCCNT_TIMESTAMP
  #define USE_CYCCNT_TIMESTAMP    1
#endif

// Define as 1 if the Cortex-M cycle counter is used and there might be no debugger attached while recording.
#ifndef   ENABLE_DWT_CYCCNT
  #define ENABLE_DWT_CYCCNT       (USE_CYCCNT_TIMESTAMP & SEGGER_SYSVIEW_POST_MORTEM_MODE)
#endif

/*********************************************************************
*
*       Defines, fixed
*
**********************************************************************
*/
#define DEMCR                     (*(volatile unsigned long*) (0xE000EDFCuL))   // Debug Exception and Monitor Control Register
#define TRACEENA_BIT              (1uL << 24)                                   // Trace enable bit
#define DWT_CTRL                  (*(volatile unsigned long*) (0xE0001000uL))   // DWT Control Register
#define NOCYCCNT_BIT              (1uL << 25)                                   // Cycle counter support bit
#define CYCCNTENA_BIT             (1uL << 0)                                    // Cycle counter enable bit

/********************************************************************* 
*
*       _cbSendSystemDesc()
*
*  Function description
*    Sends SystemView description strings.
*/
static void _cbSendSystemDesc(void) {
  SEGGER_SYSVIEW_SendSysDesc("N="SYSVIEW_APP_NAME",D="SYSVIEW_DEVICE_NAME);
    SEGGER_SYSVIEW_SendSysDesc("I#15=SysTick,"
                               "I#16=eDMA0_16,"
                               "I#17=eDMA1_17,"
                               "I#18=eDMA2_18,"
                               "I#19=eDMA3_19,"
                               "I#20=eDMA4_20,"
                               "I#21=eDMA5_21,"
                               "I#22=eDMA6_22,");
    SEGGER_SYSVIEW_SendSysDesc("I#23=eDMA7_23,"
                               "I#24=eDMA8_24,"
                               "I#25=eDMA9_25,"
                               "I#26=eDMA10_26,"
                               "I#27=eDMA11_27,"
                               "I#28=eDMA12_28,"
                               "I#29=eDMA13_29,");
    SEGGER_SYSVIEW_SendSysDesc("I#30=eDMA14_30,"
                               "I#31=eDMA15_31,"
                               "I#32=eDMA_error,"
                               "I#33=CM7_CTI0,"
                               "I#34=CM7_CTI1,"
                               "I#35=CM7_CorePlatform,"
                               "I#36=LPUART1,"
                               "I#37=LPUART2,");
    SEGGER_SYSVIEW_SendSysDesc("I#38=LPUART3,"
                               "I#39=LPUART4,"
                               "I#40=LPUART5,"
                               "I#41=LPUART6,"
                               "I#42=LPUART7,"
                               "I#43=LPUART8,"
                               "I#44=LPI2C1,"
                               "I#45=LPI2C2,"
                               "I#46=LPI2C3,");
    SEGGER_SYSVIEW_SendSysDesc("I#47=LPI2C4,"
                               "I#48=LPSPI1,"
                               "I#49=LPSPI2,"
                               "I#50=LPSPI3,"
                               "I#51=LPSPI4,"
                               "I#52=FlexCAN1,"
                               "I#53=FlexCAN2,");
    SEGGER_SYSVIEW_SendSysDesc("I#54=FlexRAM,"
                               "I#55=KPP,"
                               "I#56=TSC,"
                               "I#57=GPR,"
                               "I#58=LCDIF,"
                               "I#59=CSI,"
                               "I#60=PXP,"
                               "I#61=WDOG2,");
    SEGGER_SYSVIEW_SendSysDesc("I#62=SNVS_HP_WRAPPER,"
                               "I#63=SNVS_HP_WRAPPER_Security,"
                               "I#64=SNVS_LPHP_WRAPPER,"
                               "I#65=CSU,"
                               "I#66=DCP,"
                               "I#67=DCP_CH0,"
                               "I#69=TRNG,");
    SEGGER_SYSVIEW_SendSysDesc("I#71=BEE,"
                               "I#72=SAI1,"
                               "I#73=SAI2,"
                               "I#74=SAI3_RX,"
                               "I#75=SAI3_TX,"
                               "I#76=SPDIF,"
                               "I#77=PMU,"
                               "I#79=TEMP,");
    SEGGER_SYSVIEW_SendSysDesc("I#80=TEMP_PANIC,"
                               "I#81=USBPHY0,"
                               "I#82=USBPHY1,"
                               "I#83=ADC1,"
                               "I#84=ADC2,"
                               "I#85=DCDC,"
                               "I#88=GPIO1_INT0,");
    SEGGER_SYSVIEW_SendSysDesc("I#89=GPIO1_INT1,"
                               "I#90=GPIO1_INT2,"
                               "I#91=GPIO1_INT3,"
                               "I#92=GPIO1_INT4,"
                               "I#93=GPIO1_INT5,");
    SEGGER_SYSVIEW_SendSysDesc("I#94=GPIO1_INT6,"
                               "I#95=GPIO1_INT7,"
                               "I#96=GPIO1_0_15,"
                               "I#97=GPIO1_16_31,"
                               "I#98=GPIO2_0_15,");
    SEGGER_SYSVIEW_SendSysDesc("I#99=GPIO2_16_31,"
                               "I#100=GPIO3_0_15,"
                               "I#101=GPIO3_16_31,"
                               "I#102=GPIO4_0_15,"
                               "I#103=GPIO4_16_31,"
                               "I#104=GPIO5_0_15,"
                               "I#105=GPIO5_16_31,");
    SEGGER_SYSVIEW_SendSysDesc("I#106=FLEXIO1,"
                               "I#107=FLEXIO2,"
                               "I#108=WDOG1,"
                               "I#109=RTWDOG,"
                               "I#110=EWM,"
                               "I#111=CCM1,"
                               "I#112=CCM2,"
                               "I#113=GPC,"
                               "I#114=SRC,"
                               "I#116=GPT1,");
    SEGGER_SYSVIEW_SendSysDesc("I#117=GPT2,"
                               "I#118=FlexPWM1_0,"
                               "I#119=FlexPWM1_1,"
                               "I#120=FlexPWM1_2,"
                               "I#121=FlexPWM1_3,"
                               "I#122=FlexPWM1_Fault,"
                               "I#123=FlexSPI2,");
    SEGGER_SYSVIEW_SendSysDesc("I#124=FlexSPI1,"
                               "I#125=SEMC,"
                               "I#126=USDHC1,"
                               "I#127=USDHC2,"
                               "I#128=USB_OTG2,"
                               "I#129=USB_OTG1,"
                               "I#130=ENET,"
                               "I#131=ENET_1588,");
    SEGGER_SYSVIEW_SendSysDesc("I#132=XBAR1_0_1,"
                               "I#133=XBAR1_2_3,"
                               "I#134=ADC_ETC_0,"
                               "I#135=ADC_ETC_1,"
                               "I#136=ADC_ETC_2,"
                               "I#137=ADC_ETC_Error,"
                               "I#138=PIT,");
    SEGGER_SYSVIEW_SendSysDesc("I#139=ACMP_1,"
                               "I#140=ACMP_2,"
                               "I#141=ACMP_3,"
                               "I#142=ACMP_4,"
                               "I#145=ENC1,"
                               "I#146=ENC2,"
                               "I#147=ENC3,"
                               "I#148=ENC4,"
                               "I#149=QTIMER1,"
                               "I#150=QTIMER2,");
    SEGGER_SYSVIEW_SendSysDesc("I#151=QTIMER3,"
                               "I#152=QTIMER4,"
                               "I#153=FlexPWM2_0,"
                               "I#154=FlexPWM2_1,"
                               "I#155=FlexPWM2_2,"
                               "I#156=FlexPWM2_3,"
                               "I#157=FlexPWM2_Fault,");
    SEGGER_SYSVIEW_SendSysDesc("I#158=FlexPWM3_0,"
                               "I#159=FlexPWM3_1,"
                               "I#160=FlexPWM3_2,"
                               "I#161=FlexPWM3_3,"
                               "I#162=FlexPWM3_Fault,"
                               "I#163=FlexPWM4_0,"
                               "I#164=FlexPWM4_1,");
    SEGGER_SYSVIEW_SendSysDesc("I#165=FlexPWM4_2,"
                               "I#166=FlexPWM4_3,"
                               "I#167=FlexPWM4_Fault,"
                               "I#168=ENET2,"
                               "I#169=ENET2_1588,"
                               "I#170=FLEXCAN3,"
                               "I#171=FLEXIO3,");
    SEGGER_SYSVIEW_SendSysDesc("I#172=GPIO6_7_8_9");
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/
void SEGGER_SYSVIEW_Conf(void) {
#if USE_CYCCNT_TIMESTAMP
#if ENABLE_DWT_CYCCNT
  //
  // If no debugger is connected, the DWT must be enabled by the application
  //
  if ((DEMCR & TRACEENA_BIT) == 0) {
    DEMCR |= TRACEENA_BIT;
  }
#endif
  //
  //  The cycle counter must be activated in order
  //  to use time related functions.
  //
  if ((DWT_CTRL & NOCYCCNT_BIT) == 0) {       // Cycle counter supported?
    if ((DWT_CTRL & CYCCNTENA_BIT) == 0) {    // Cycle counter not enabled?
      DWT_CTRL |= CYCCNTENA_BIT;              // Enable Cycle counter
    }
  }
#endif
  SEGGER_SYSVIEW_Init(SYSVIEW_TIMESTAMP_FREQ, SYSVIEW_CPU_FREQ, 
                      0, _cbSendSystemDesc);
  SEGGER_SYSVIEW_SetRAMBase(SYSVIEW_RAM_BASE);
}

/*************************** End of file ****************************/
