
#include <reent.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <SEGGER/SEGGER_RTT.h>

char *__env[1] = {0};
char **environ = __env;

extern "C" void _exit(int rc)
{
    asm("BKPT");
    while (true)
        ;
}

extern "C" int _close_r(struct _reent *reent, int)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _execve_r(struct _reent *reent, const char *, char *const *, char *const *)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _fcntl_r(struct _reent *reent, int, int, int)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _fork_r(struct _reent *reent)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _fstat_r(struct _reent *reent, int, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" int _getpid_r(struct _reent *reent)
{
    return 1;
}

extern "C" int _isatty_r(struct _reent *reent, int)
{
    __errno_r(reent) = ENOSYS;
    return 0;
}

extern "C" int _kill_r(struct _reent *reent, int, int)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _link_r(struct _reent *reent, const char *, const char *)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" _off_t _lseek_r(struct _reent *reent, int, _off_t, int)
{
    return 0;
}

extern "C" int _mkdir_r(struct _reent *reent, const char *, int)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _open_r(struct _reent *reent, const char *, int, int)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" _ssize_t _read_r(struct _reent *reent, int fd, void * buffer, size_t size)
{
    switch(fd)
    {
    case 0:
        size = SEGGER_RTT_Read(0, buffer,size);
        return size;
    default:
        __errno_r(reent) = EBADF;
        return -1;
    }
}

extern "C" int _rename_r(struct _reent *reent, const char *, const char *)
{
    __errno_r(reent) = ENOSYS;
    return 0;
}

extern "C" void *_sbrk_r(struct _reent *reent, ptrdiff_t incr)
{
    extern char _heap_start; /* Defined by the linker */
    static char *heap_end;
    char *prev_heap_end;
    if (heap_end == 0)
    {
        heap_end = &_heap_start;
    }
    prev_heap_end = heap_end;
    heap_end += incr;
    return (caddr_t)prev_heap_end;
}

extern "C" int _stat_r(struct _reent *reent, const char *, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" _CLOCK_T_ _times_r(struct _reent *reent, struct tms *)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" int _unlink_r(struct _reent *reent, const char *)
{
    __errno_r(reent) = ENOSYS;
    return 0;
}

extern "C" int _wait_r(struct _reent *reent, int *)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}

extern "C" _ssize_t _write_r(struct _reent *reent, int fd, const void *buffer, size_t size)
{
    switch(fd)
    {
    case 1:
    case 2:
        size = SEGGER_RTT_Write(0, buffer,size);
        return size;
    default:
        __errno_r(reent) = EBADF;
        return -1;
    }
}

/* This one is not guaranteed to be available on all targets.  */
extern "C" int gettimeofday_r(struct _reent *reent, struct timeval *__tp, void *__tzp)
{
    __errno_r(reent) = ENOSYS;
    return -1;
}