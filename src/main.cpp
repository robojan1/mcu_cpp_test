#include <board.h>
#include <fsl_gpio.h>
#include <stdio.h>
#include <SEGGER/SEGGER_SYSVIEW.h>

int main() 
{
    board_init();
    SEGGER_SYSVIEW_Conf();

    gpio_pin_config_t config{
        .direction = kGPIO_DigitalOutput,
        .outputLogic = 0,
        .interruptMode = kGPIO_NoIntmode
    };

    GPIO_PinInit(GPIO2, 3, &config);
    while(true)
    {
        GPIO_PortToggle(GPIO2, 1 << 3);
        printf("Hello world!!!\n");
        for(int i = 0; i < 20000000; i++);
    }
    return 0;
}