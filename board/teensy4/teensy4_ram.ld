/*
** ###################################################################
**     Processors:          MIMXRT1062CVJ5A
**                          MIMXRT1062CVL5A
**                          MIMXRT1062DVJ6A
**                          MIMXRT1062DVL6A
**
**     Compiler:            GNU C Compiler
**     Reference manual:    IMXRT1060RM Rev. B, 07/2018
**     Version:             rev. 0.1, 2017-01-10
**     Build:               b180801
**
**     Abstract:
**         Linker file for the GNU C Compiler
**
**     Copyright 2016 Freescale Semiconductor, Inc.
**     Copyright 2016-2018 NXP
**
**     SPDX-License-Identifier: BSD-3-Clause
**
**     http:                 www.nxp.com
**     mail:                 support@nxp.com
**
** ###################################################################
*/

/* Entry Point */
ENTRY(Reset_Handler)

STACK_SIZE = DEFINED(__stack_size__) ? __stack_size__ : 0x1000;

/*
 * When you change these sizes also change the memory protection
 */

RAM_SIZE   = 0x100000;
ITCM_SIZE  = 0x040000;
DTCM_SIZE  = 0x040000;
OCRAM_SIZE = RAM_SIZE - ITCM_SIZE - DTCM_SIZE;

ASSERT(OCRAM_SIZE >= 0, "Invalid RAM size settings")

/* Specify the program headers */
/*PHDRS
{
  headers PT_PHDR 
}*/

/* Specify the memory areas */
MEMORY
{
  m_interrupts          (RXI)  : ORIGIN = 0x00000000, LENGTH = 0x00000400
  m_itcm                (RXI)  : ORIGIN = 0x00000400, LENGTH = (ITCM_SIZE - 0x400)
  m_dtcm                (RWI)  : ORIGIN = 0x20000000, LENGTH = DTCM_SIZE
  m_ocram               (RW)  : ORIGIN = 0x20200000, LENGTH = OCRAM_SIZE
}

/* Define output sections */
SECTIONS
{
  /* The startup code goes first into internal RAM */
  .interrupts :
  {
    __VECTOR_TABLE = .;
    . = ALIGN(4);
    KEEP(*(.isr_vector))     /* Startup code */
    . = ALIGN(4);
  } > m_interrupts

  __VECTOR_RAM = __VECTOR_TABLE;
  __RAM_VECTOR_TABLE_SIZE_BYTES = 0x0;


  /* The program code and other data goes into internal RAM */
  .text :
  {
    . = ALIGN(4);
    KEEP(*(.text.ostable))
    *(.startup)
    *(.text)                 /* .text sections (code) */
    *(.text*)                /* .text* sections (code) */
    *(.glue_7)               /* glue arm to thumb code */
    *(.glue_7t)              /* glue thumb to arm code */
    *(.eh_frame)
    KEEP (*(.init))
    KEEP (*(.fini))
    . = ALIGN(4);
  } > m_itcm

  .ARM.extab :
  {
    *(.ARM.extab* .gnu.linkonce.armextab.*)
  } > m_itcm

  .ARM :
  {
    __exidx_start = .;
    *(.ARM.exidx*)
    __exidx_end = .;
  } > m_itcm

 .ctors :
  {
    __CTOR_LIST__ = .;
    /* gcc uses crtbegin.o to find the start of
       the constructors, so we make sure it is
       first.  Because this is a wildcard, it
       doesn't matter if the user does not
       actually link against crtbegin.o; the
       linker won't look for a file to match a
       wildcard.  The wildcard also means that it
       doesn't matter which directory crtbegin.o
       is in.  */
    KEEP (*crtbegin.o(.ctors))
    KEEP (*crtbegin?.o(.ctors))
    /* We don't want to include the .ctor section from
       from the crtend.o file until after the sorted ctors.
       The .ctor section from the crtend file contains the
       end of ctors marker and it must be last */
    KEEP (*(EXCLUDE_FILE(*crtend?.o *crtend.o) .ctors))
    KEEP (*(SORT(.ctors.*)))
    KEEP (*(.ctors))
    __CTOR_END__ = .;
  } > m_itcm

  .dtors :
  {
    __DTOR_LIST__ = .;
    KEEP (*crtbegin.o(.dtors))
    KEEP (*crtbegin?.o(.dtors))
    KEEP (*(EXCLUDE_FILE(*crtend?.o *crtend.o) .dtors))
    KEEP (*(SORT(.dtors.*)))
    KEEP (*(.dtors))
    __DTOR_END__ = .;
  } > m_itcm

  .preinit_array :
  {
    PROVIDE_HIDDEN (__preinit_array_start = .);
    KEEP (*(.preinit_array*))
    PROVIDE_HIDDEN (__preinit_array_end = .);
  } > m_itcm

  .init_array :
  {
    PROVIDE_HIDDEN (__init_array_start = .);
    KEEP (*(SORT(.init_array.*)))
    KEEP (*(.init_array*))
    PROVIDE_HIDDEN (__init_array_end = .);
  } > m_itcm

  .fini_array :
  {
    PROVIDE_HIDDEN (__fini_array_start = .);
    KEEP (*(SORT(.fini_array.*)))
    KEEP (*(.fini_array*))
    PROVIDE_HIDDEN (__fini_array_end = .);
  } > m_itcm

  .rodata :
  {
    . = ALIGN(4);
    __rodata_start__ = .;
    *(.rodata)               /* .rodata sections (constants, strings, etc.) */
    *(.rodata*)              /* .rodata* sections (constants, strings, etc.) */
    . = ALIGN(4);
    __rodata_end__ = .;
  } > m_itcm

  __etext = .;    /* define a global symbol at end of code */
  __DATA_ROM = .; /* Symbol is used by startup for data initialization */

  .data :
  {
    . = ALIGN(4);
    __DATA_RAM = .;
    __data_start__ = .;      /* create a global symbol at data start */
    *(m_usb_dma_init_data)
    *(.data)                 /* .data sections */
    *(.data*)                /* .data* sections */
    KEEP(*(.jcr*))
    . = ALIGN(4);
    __data_end__ = .;        /* define a global symbol at data end */
  } > m_dtcm

  __NDATA_ROM = __DATA_ROM + (__data_end__ - __data_start__);

  .ncache.init :
  {
    __noncachedata_start__ = .;   /* create a global symbol at ncache data start */
    *(NonCacheable.init)
    . = ALIGN(4);
    __noncachedata_init_end__ = .;   /* create a global symbol at initialized ncache data end */
  } > m_dtcm

  . = __noncachedata_init_end__;
  .ncache :
  {
    *(NonCacheable)
    . = ALIGN(4);
    __noncachedata_end__ = .;     /* define a global symbol at ncache data end */
  } > m_dtcm

  __DATA_END = __NDATA_ROM + (__noncachedata_init_end__ - __noncachedata_start__);
  text_end = ORIGIN(m_itcm) + LENGTH(m_itcm);
  ASSERT(__DATA_END <= text_end, "region m_itcm overflowed with text and data")

  /* Uninitialized data section */
  .bss :
  {
    /* This is used by the startup in order to initialize the .bss section */
    . = ALIGN(4);
    __START_BSS = .;
    __bss_start__ = .;
    *(m_usb_dma_noninit_data)
    *(.bss)
    *(.bss*)
    *(COMMON)
    . = ALIGN(4);
    __bss_end__ = .;
    __END_BSS = .;
  } > m_dtcm

  .stack :
  {
    . = ALIGN(8);
    PROVIDE(__StackLimit = .);
    . += STACK_SIZE;
    __StackTop = .;
  } > m_dtcm

  .fastheap :
  {
    . = ALIGN(8);
    __end__ = .;
    end = .;
    __FastHeapBase = .;
  } > m_dtcm

  __FastHeapEnd = ORIGIN(m_dtcm) + LENGTH(m_dtcm) - 1;
  __FastHeapSize = __FastHeapEnd - __FastHeapBase + 1;

  .heap :
  {
    . = ALIGN(8);
    __HeapBase = .;
    __HeapEnd = ORIGIN(m_ocram) + LENGTH(m_ocram) - 1;
    __HeapSize = __HeapEnd - __HeapBase + 1;
  } > m_ocram


  /* Initializes stack on the end of block */
  __stack = __StackTop;  
	_itcm_block_count = (__etext + 0x7FFF) >> 15;
	_flexram_bank_config = 0xAAAAAAAA | ((1 << (_itcm_block_count * 2)) - 1);

	_heap_start = __FastHeapBase;
	_heap_end = __FastHeapEnd;
  
	__data_start__ = ADDR(.data);
	__data_end__ = ADDR(.data) + SIZEOF(.data);
	__data_load__ = LOADADDR(.data);

  .ARM.attributes 0 : { *(.ARM.attributes) }
}

