#ifndef BOARD_H
#define BOARD_H

#include <MIMXRT1062.h>
#include <MIMXRT1062_features.h>
#include <board/pin_mux.h>
#include <board/clock_config.h>
#include <board/dcd.h>

#define BOARD_FLASH_SIZE 0x1F0000U

#ifdef __cplusplus
extern "C" {
#endif

void board_init(void);

#ifdef __cplusplus
}
#endif

#endif